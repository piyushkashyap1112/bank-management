//Sum of all elements of Array

#include <iostream>
using namespace std;

int arSum(int *arr,int size){
    if(size==0 ){
        return 0;
    }
    
    if(size==1){
        return arr[0];   //if only one elmnt ,then its the sum
    }
    
    int remainingPart=arSum(arr+1,size-1); //remainingPart of the array's arr[0] is added one by one 
    int sum=arr[0]+ remainingPart;          //to get sum
    
    return sum;
    
}


int main() {
	int arr[]={1,3,10,4,5};
	int size=5;
	
	int sum=arSum(arr,size);
	cout<<sum;
	return 0;
}